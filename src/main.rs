#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(crap_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

use bootloader::{BootInfo, entry_point};
use core::panic::PanicInfo;

mod vga_buffer;
mod serial;

static HELLO: &[u8] = b"Hello World!";

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    // TODO using the write*! flavor of macros is currently causing errors for some reason (qemu keeps rebooting)
    println!("Hello World{}", "!");
    println!(", some numbers: {} {}", 42, 1.337);

    crap_os::init();

    use x86_64::registers::control::Cr3;

    let (level_4_page_table, _) = Cr3::read();
    println!("Level 4 page table at: {:?}", level_4_page_table.start_address());

    // invoke a breakpoint exception
    // x86_64::instructions::interrupts::int3();

    // trigger a page fault
//    unsafe {
//        *(0xdeadbeef as *mut u64) = 42;
//    };

//    fn stack_overflow() {
//        stack_overflow(); // for each recursion, the return address is pushed
//    }
//
//    // trigger a stack overflow
//    stack_overflow();

//    let ptr = 0xdeadbeaf as *mut u32;
//    let ptr = 0x205868 as *mut u32;
    // read from a code page
//    unsafe { let x = *ptr; }
//    println!("read worked");

    // write to a code page
//    unsafe { *ptr = 42; }
//    spoilers, the write won't work...
//    println!("write worked");
//    unsafe { *ptr = 42; }

    use crap_os::memory::active_level_4_table;
    use x86_64::VirtAddr;

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let l4_table = unsafe { active_level_4_table(phys_mem_offset) };

    for (i, entry) in l4_table.iter().enumerate() {
        if !entry.is_unused() {
            println!("L4 Entry {}: {:?}", i, entry);
        }
    }

    #[cfg(test)]
    test_main();

    println!("It did not crash!");
    crap_os::hlt_loop();
}

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    crap_os::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    crap_os::test_panic_handler(info)
}

#[test_case]
fn trivial_assertion() {
    serial_print!("trivial assertion... ");
    assert_eq!(1, 1);
    serial_println!("[ok]");
}
